import { sync } from 'glob'
import { resolve } from 'path'
import { Configuration } from 'webpack'

import entries from './.webpack.d/entries'

// all default export from webpack.*.d must be with following type
export type ConfigurationExtender = (configuration: Configuration) => void

// used by webpack-dev-server for URL rewrites
const entryUrlRewrites = entries.filter(({ name }) => name !== 'index').map(({ name }) => ({
  from: RegExp(`/${name}`),
  to: `/${name}.html`,
}))

export default (env: string = 'dev'): Configuration => {
  const configuration: Configuration = {
    resolve: {
      // Add '.ts' and '.tsx' as resolvable extensions.
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
      // path to lookup for import
      modules: [resolve(__dirname), 'node_modules'],
    },

    context: __dirname,

    // mode
    mode: env === 'dev' ? 'development' : 'production',

    // empty configs
    module: { rules: [] },
    optimization: {},
    plugins: [],

    // for webpack-dev-server only
    devServer: {
      compress: false,
      contentBase: resolve(__dirname, 'public'),
      historyApiFallback: {
        rewrites: entryUrlRewrites,
      },
      host: '0.0.0.0',
      port: 8888,
    },
  }

  // extend configuration based on env accordingly
  sync(`./.webpack.${env}.d/*.ts`).forEach((filePath) => require(filePath).default(configuration))

  return configuration
}
