FROM nginx:alpine

# Configure nginx.conf
RUN rm /etc/nginx/conf.d/*
COPY nginx.conf /etc/nginx/conf.d/

# Copy static files to default nginx html place
COPY dist /etc/nginx/html