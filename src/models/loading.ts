import { createAction, createReducer } from 'redux-act'
import { createSelector } from 'reselect'

/**
 * -----------------------------------------------------------------
 * State
 */
export interface ILoadingState {
  isLoading: boolean
}

const defaultState: ILoadingState = {
  isLoading: false,
}

/**
 * -----------------------------------------------------------------
 * Selector
 */
const stateSelector = (state: any) => state.loading
export const selectorIsLoading = createSelector(stateSelector, (state: ILoadingState) => state.isLoading)

/**
 * -----------------------------------------------------------------
 * Actions
 */
export const actionTurnOnLoading = createAction('actionTurnOnLoading')
export const actionTurnOffLoading = createAction('actionTurnOffLoading')

/**
 * -----------------------------------------------------------------
 * Reducers
 */
const reducer = createReducer<typeof defaultState>({}, defaultState)

reducer.on(actionTurnOnLoading, (state: ILoadingState) => ({
  ...state,
  isLoading: true,
}))

reducer.on(actionTurnOffLoading, (state: ILoadingState) => ({
  ...state,
  isLoading: false,
}))

export default reducer
