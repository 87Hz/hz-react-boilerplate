import { createAction, createReducer } from 'redux-act'
import { createSelector } from 'reselect'

/**
 * -----------------------------------------------------------------
 * State
 */
export interface ICounterState {
  value: number
  input: number
}

const defaultState: ICounterState = {
  input: 0,
  value: 0,
}

/**
 * -----------------------------------------------------------------
 * Selector
 */
const stateSelector = (state: any) => state.counter
export const selectorValue = createSelector(stateSelector, (state: ICounterState) => state.value)
export const selectorInput = createSelector(stateSelector, (state: ICounterState) => state.input)

/**
 * -----------------------------------------------------------------
 * Actions
 */
export const actionInc = createAction('actionInc')
export const actionDec = createAction('actionDec')
export const actionReset = createAction('actionReset')
export const actionAdd = createAction<number>('actionAdd')
export const actionUpdateInput = createAction<number>('actionUpdateInput')

/**
 * -----------------------------------------------------------------
 * Reducers
 */
const reducer = createReducer<typeof defaultState>({}, defaultState)

reducer.on(actionInc, (state: ICounterState) => ({
  ...state,
  value: state.value + 1,
}))

reducer.on(actionDec, (state: ICounterState) => ({
  ...state,
  value: state.value - 1,
}))

reducer.on(actionAdd, (state: ICounterState, payload) => ({
  ...state,
  value: state.value + payload,
}))

reducer.on(actionUpdateInput, (state: ICounterState, payload) => ({
  ...state,
  input: payload,
}))

reducer.on(actionReset, (state: ICounterState) => ({
  ...state,
  value: 0,
}))

export default reducer
