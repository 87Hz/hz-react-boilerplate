import { routeNodeSelector, RouterState } from 'redux-router5'
import { createSelector } from 'reselect'

/**
 * -----------------------------------------------------------------
 * Selector
 */
const stateSelector = routeNodeSelector('')
export const selectorRouteName = createSelector(stateSelector, (state: RouterState) => state.route.name)
