import { createAction, createReducer } from 'redux-act'
import { delay } from 'redux-saga'
import { put } from 'redux-saga/effects'
import { createSelector } from 'reselect'

// import { axiosClient } from 'src/utils/axios'
import createSagaWatcher, { IModelSaga } from 'src/utils/createSagaWatcher'
import { log } from 'src/utils/logger'

/**
 * -----------------------------------------------------------------
 * State
 */
export interface IRemoteState {
  message: string
}

const defaultState: IRemoteState = {
  message: '',
}

/**
 * -----------------------------------------------------------------
 * Selector
 */
const stateSelector = (state: any) => state.remote
export const selectorMessage = createSelector(stateSelector, (state: IRemoteState) => state.message)

/**
 * -----------------------------------------------------------------
 * Actions
 */
export const actionSetMessage = createAction<string>('actionSetMessage')
export const actionLoadRemote = createAction<string>('actionLoadRemote')

/**
 * -----------------------------------------------------------------
 * Sagas
 */
const saga: IModelSaga = {
  *[actionLoadRemote.getType()](action) {
    const { payload } = action
    log.info('Saga function action.payload: ', payload)

    yield put(actionSetMessage('正在加载'))

    // real API call
    // const res = yield axiosClient.get('')

    // simulate API call
    const res = { data: { message: 'Hello, World!' } }
    yield delay(2000)

    yield put(actionSetMessage(res.data.message))
  },
}

export const sagaWatcher = createSagaWatcher(saga, { withLoading: true })

/**
 * -----------------------------------------------------------------
 * Reducers
 */
const reducer = createReducer<typeof defaultState>({}, defaultState)

reducer.on(actionSetMessage, (state: IRemoteState, payload: string) => ({
  ...state,
  message: payload,
}))

export default reducer
