import { IDefaultRoute, IRoute, IRouteConfig } from 'src/utils/createRouter'

const routes: IRoute[] = [{ name: 'home', path: '/' }, { name: 'another', path: '/another' }]

const defaultRoute: IDefaultRoute = {
  defaultRoute: 'home',
}

export const routeConfig: IRouteConfig = { defaultRoute, routes, base: '' }
