import React from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux-router5'
import { startsWithSegment } from 'router5-helpers'

import { selectorRouteName } from 'src/models/router'

import AnotherRoute from './screens/AnotherRoute'

/**
 * ----------------------------
 * sFun
 */
interface ISProps {
  routeName: string
}
const sFun: (s: any) => ISProps = (s) => ({
  routeName: selectorRouteName(s),
})

/**
 * ----------------------------
 * dFun
 */
interface IDProps {
  navTo: (routeName: string, params?: object) => () => void
  toEntry: (entryName: string) => () => {}
}

const dFn: (d: any) => IDProps = (d) => ({
  navTo: (routeName: string, params: object = {}) => () => d(actions.navigateTo(routeName, params)),
  toEntry: (entryName: string) => () => (location.href = entryName),
})

/**
 * ----------------------------
 * component
 */
interface IProps extends ISProps, IDProps {}

const Component = (props: IProps) => {
  const { navTo, toEntry, routeName } = props
  const testRoute = startsWithSegment(routeName)

  return (
    <div>
      <h1>This is the Homepage. 这是主页。</h1>
      <ul>
        <li>
          <button onClick={toEntry('post-login')}>Redirect to another entry: post-login</button>
        </li>
        <li>
          <button onClick={navTo('another')}>Same entry: another</button>
        </li>
        <li>
          <button onClick={navTo('home')}>Same entry: home</button>
        </li>
      </ul>

      {testRoute('another') && <AnotherRoute />}
    </div>
  )
}

export default connect(sFun, dFn)(Component)
