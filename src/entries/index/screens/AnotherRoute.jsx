import React from 'react'

export default () => (
  <div>
    <p>This is another route，screen written in .jsx file, not .tsx (no type checking).</p>
    <p>这是另外一个路由，页面是jsx文件写的，不是tsx (所以没有类型检查).</p>
  </div>
)
