import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import { RouterProvider } from 'react-router5'

import createRouter from 'src/utils/createRouter'
import createStore from 'src/utils/createStore'
import { log } from 'src/utils/logger'

import App from './app'
import { routeConfig } from './routeConfig'

const appRouter = createRouter(routeConfig)
const store = createStore(appRouter)

// add store as dependency
appRouter.setDependencies({ store, log })
// start router when page loads
appRouter.start(/* can put initial route name here */)

// main render function
const render = (Component: any) => {
  ReactDOM.render(
    <AppContainer warnings={false}>
      <Provider store={store}>
        <RouterProvider router={appRouter}>
          <Component />
        </RouterProvider>
      </Provider>
    </AppContainer>,
    document.getElementById('app'),
  )
}

// render App
render(App)

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./app', () => {
    render(App)
  })
}
