import { Button, InputItem } from 'antd-mobile'
import React from 'react'
import { connect } from 'react-redux'
import { batch } from 'redux-act'

import {
  actionAdd,
  actionDec,
  actionInc,
  actionReset,
  actionUpdateInput,
  selectorInput,
  selectorValue,
} from 'src/models/counter'

/**
 * ----------------------------
 * sFun
 */
interface ISProps {
  s: any

  input: number
  value: number
}

const sFn: (s: any) => ISProps = (s) => ({
  s,

  input: selectorInput(s),
  value: selectorValue(s),
})

/**
 * ----------------------------
 * dFun
 */
interface IDProps {
  d

  add: (value: number) => () => void
  dec: () => void
  inc: () => void
  reset: () => void
  update: (value: string) => void
}

const dFn: (d: any) => IDProps = (d) => ({
  d,

  add: (value: number) => () => d(actionAdd(value)),
  dec: () => d(actionDec()),
  inc: () => d(actionInc()),
  reset: () => d(batch(actionReset(), actionUpdateInput(0))),
  update: (value: string) => {
    const parsedValue: number = value === '' ? 0 : Number.parseInt(value)
    if (!isNaN(parsedValue)) {
      d(actionUpdateInput(parsedValue))
    }
  },
})

/**
 * ----------------------------
 * component
 */
interface IProps extends ISProps, IDProps {}

export const Component = (props: IProps) => {
  const { value, input, update, dec, inc, add, reset } = props

  return (
    <div>
      Value: {value}
      <InputItem value={input.toString()} onChange={update} />
      <div>
        <Button size="small" inline onClick={dec}>
          DEC
        </Button>
        <Button size="small" inline onClick={inc}>
          INC
        </Button>
        <Button size="small" type="primary" inline onClick={add(input)}>
          ADD
        </Button>
        <Button size="small" type="warning" inline onClick={reset}>
          RESET
        </Button>
      </div>
    </div>
  )
}

export const Container = connect(sFn, dFn)(Component)
