import React from 'react'

import { Container as Counter } from './components/Counter'

const Screen = () => {
  return (
    <div>
      <Counter />
    </div>
  )
}

export default Screen
