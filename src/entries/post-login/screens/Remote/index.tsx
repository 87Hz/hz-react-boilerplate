import React from 'react'
import { connect } from 'react-redux'

import { selectorIsLoading } from 'src/models/loading'
import { actionLoadRemote, selectorMessage } from 'src/models/remote'

/**
 * ----------------------------
 * sFun
 */
interface ISProps {
  isLoading: boolean
  message: string
}

const sFn: (s: any) => ISProps = (s) => ({
  isLoading: selectorIsLoading(s),
  message: selectorMessage(s),
})

/**
 * ----------------------------
 * dFun
 */
interface IDProps {
  handleButtonClick: () => void
}

const dFn: (d: any) => IDProps = (d) => ({
  handleButtonClick: () => d(actionLoadRemote('data from remote screen')),
})

/**
 * ----------------------------
 * component
 */
interface IProps extends ISProps, IDProps { }

const Screen = (props: IProps) => {
  const { message, isLoading, handleButtonClick } = props

  return (
    <div>
      <p>{message}</p>
      <p>isLoading: {isLoading ? 'true' : 'false'}</p>
      <button onClick={handleButtonClick}>Load remote || 点击加载</button>
    </div>
  )
}

export default connect(sFn, dFn)(Screen)
