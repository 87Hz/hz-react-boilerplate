import React from 'react'
import { connect } from 'react-redux'
import { startsWithSegment } from 'router5-helpers'

import { Container as NavBar } from 'src/components/NavBar'
import { selectorRouteName } from 'src/models/router'
import Counter from './screens/Counter'
import Home from './screens/Home'
import Remote from './screens/Remote'

/**
 * ----------------------------
 * sFun
 */
interface ISProps {
  routeName: string
}
const sFun: (s: any) => ISProps = (s) => ({
  routeName: selectorRouteName(s),
})

/**
 * ----------------------------
 * component
 */
interface IProps extends ISProps {}

const App = (props: IProps) => {
  const { routeName } = props
  const testRoute = startsWithSegment(routeName)

  return (
    <div>
      <NavBar />

      {testRoute('home') && <Home />}
      {testRoute('remote') && <Remote />}
      {testRoute('counter') && <Counter />}
    </div>
  )
}

export default connect(sFun)(App)
