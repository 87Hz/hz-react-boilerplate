import { Router, State } from 'router5'

import { selectorMessage } from 'src/models/remote'
import { IDefaultRoute, IRoute, IRouteConfig, IRouterDependencies, RouteInitFn } from 'src/utils/createRouter'
import { log } from 'src/utils/logger'

const routes: IRoute[] = [
  { name: 'home', path: '/' },
  { name: 'remote', path: '/remote' },

  {
    name: 'about',
    path: '/about/:who',

    onEnter: ({ state }) => log.info('Enter: about, this is state', state),
    onLeave: ({ params }) => log.info('Leave: about, this is params', params),
  },

  /**
   * --------------------------------------------------------------
   * counter
   */
  {
    name: 'counter',
    path: '/counter',
  },
  {
    name: 'counter.private',
    path: '/private',

    canActivate: (router: Router) => (_, fromState?: State) => {
      if (fromState === null) {
        router.navigateToDefault()
      } else {
        const { name } = fromState

        if (!(name === 'home')) {
          router.navigateToDefault()
        } else {
          return true
        }
      }
    },
  },

  /**
   * --------------------------------------------------------------
   * test
   */
  {
    name: 'test',
    path: '/test',

    onEnter: () => log.info('Enter: test'),
  },
  // test.public
  {
    name: 'test.public',
    path: '/public',

    onEnter: () => log.info('Enter: test.public'),
  },
  {
    name: 'test.public.1',
    path: '/1',
  },
  {
    name: 'test.public.2',
    path: '/2',
  },
  // test.protected
  {
    name: 'test.protected',
    path: '/protected',

    onEnter: () => log.info('Enter: test.protected'),

    canActivate: (_1, dep: IRouterDependencies) => () => {
      const state = dep.store.getState()
      const remoteMessage = selectorMessage(state)
      const canEnter = remoteMessage.length !== 0

      if (!canEnter) {
        log.info('You cannot enter protected if RemoteMessage is empty')
      }

      return canEnter
    },
  },
  {
    name: 'test.protected.1',
    path: '/1',
  },
  {
    name: 'test.protected.2',
    path: '/2',
  },
]

const defaultRoute: IDefaultRoute = {
  defaultRoute: 'home',
}

const init: RouteInitFn = ({ dispatch, state }) => {
  log.info('Router init, and state is: ', state)
  log.info('Dispatched some random action, check redux-dev-tool')
  dispatch({ type: 'RANDOM_ACTION_TYPE' })
}

export const routeConfig: IRouteConfig = { routes, defaultRoute, base: '/post-login', init }
