/**
 * A component MUST:
 * has
 * - IProps: interface of presentational component props
 * exports
 * - Component: presentational component (whose props MUST be of IProps)
 * - Container: container component (use connect to get access to state and dispatch)
 *
 * e.g.
 *
 * import {
 *   Component as ComponentNameComponent,
 *   Container as ComponentNameContainer
 * } from 'components/ComponentName'
 *
 */
