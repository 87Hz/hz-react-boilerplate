import React from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux-router5'

/**
 * ----------------------------
 * dFun
 */
interface IDProps {
  navTo: (routeName: string, params?: object) => () => void
}

const dFn: (d: any) => IDProps = (d) => ({
  navTo: (routeName: string, params: object = {}) => () => d(actions.navigateTo(routeName, params)),
})

/**
 * ----------------------------
 * component
 */
interface IProps extends IDProps {}

export const Component = (props: IProps) => {
  const { navTo } = props

  return (
    <ul>
      <li onClick={navTo('home')}>Home</li>

      <li onClick={navTo('counter')}>Counter</li>
      <li onClick={navTo('counter.private')}>Counter (Private)</li>

      <li onClick={navTo('about', { who: 'me' })}>About Me</li>
      <li onClick={navTo('about', { who: 'you' })}>About You</li>

      <li onClick={navTo('remote')}>Remote</li>

      <li onClick={navTo('test')}>test</li>

      <li onClick={() => history.back()}>Back</li>

      {['public', 'public.1', 'public.2', 'protected', 'protected.1', 'protected.2'].map((routeName) => (
        <li onClick={navTo(`test.${routeName}`)} key={routeName}>
          {routeName}
        </li>
      ))}
    </ul>
  )
}

export const Container = connect(null, dFn)(Component)
