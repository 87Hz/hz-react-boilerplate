import { includes as contains } from 'lodash'
import { put, takeEvery } from 'redux-saga/effects'

import { actionTurnOffLoading, actionTurnOnLoading } from 'src/models/loading'

export interface IModelSaga {
  [actionType: string]: SagaFun
}

type SagaFun = (action: any) => IterableIterator<any>

interface ISagaWatcherConfig {
  exclude?: string[]
  include?: string[]
  withLoading?: boolean
}

const defaultSagaWatcherConfig: ISagaWatcherConfig = {
  exclude: [],
  include: [],
  withLoading: false,
}

const canSetLoading = (actionType: string, { exclude, include, withLoading }: ISagaWatcherConfig) =>
  withLoading
    ? // if withLoading is true, enabled if it is not in exclude array
    !contains(exclude, actionType)
    : // if withLoading is false, enabled if it is in include array
    contains(include, actionType)

export default (sagas, sagaWatcherConfig: ISagaWatcherConfig = defaultSagaWatcherConfig) =>
  Object.keys(sagas).map((actionType: string) =>
    (function* () {
      const saga = sagas[actionType]

      yield takeEvery(actionType, function* (args) {
        if (canSetLoading(actionType, sagaWatcherConfig)) {
          yield put(actionTurnOnLoading())
        }

        yield saga(args)

        if (canSetLoading(actionType, sagaWatcherConfig)) {
          yield put(actionTurnOffLoading())
        }
      })
    })(),
  )
