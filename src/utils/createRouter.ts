import { Dispatch, Store } from 'redux'
import createRouter, { Params, Route as Router5Route, State, transitionPath } from 'router5'
import browserPlugin from 'router5/plugins/browser'
import { Logger } from 'typescript-logger/build/logger'

export type RouteLifecycleFn = (args: { params: Params, dispatch: Dispatch<any>, state: any }) => void
export type RouteInitFn = (args: { dispatch: Dispatch<any>, state: any }) => void

export interface IRoute extends Router5Route {
  onEnter?: RouteLifecycleFn
  onLeave?: RouteLifecycleFn
}

export interface IDefaultRoute {
  defaultRoute: string
  defaultParams?: object
}

export interface IRouterDependencies {
  store: Store<any>
  log: Logger<any>
}

export interface IRouteConfig {
  routes: IRoute[]
  defaultRoute: IDefaultRoute
  base: string
  init?: RouteInitFn
}

/**
 *
 * This middleware will run lifecycle hooks when entering or leaving a route,
 * it should not stop the transition. In case of stop transition, considering
 * using canActivate function.
 *
 */
const onRouteChangeMiddleware = (routes: IRoute[]) => (_, dependencies: IRouterDependencies) => (
  toState: State,
  fromState: State,
  done,
) => {
  const { store } = dependencies
  const { toActivate, toDeactivate } = transitionPath(toState, fromState)

  toDeactivate.forEach((segment) => {
    const routeSegment = routes.find((r) => r.name === segment)

    if (routeSegment && routeSegment.onLeave) {
      routeSegment.onLeave({ params: fromState.params, dispatch: store.dispatch, state: store.getState() })
    }
  })

  toActivate.forEach((segment) => {
    const routeSegment = routes.find((r) => r.name === segment)

    if (routeSegment && routeSegment.onEnter) {
      routeSegment.onEnter({ params: toState.params, dispatch: store.dispatch, state: store.getState() })
    }
  })

  done()
}

/**
 * This middleware will run initFn when router started (fromState === null)
 */
const onRouteInitMiddleware = (init?: RouteInitFn) => (_1, dependencies: IRouterDependencies) => (
  _2,
  fromState: State,
  done
) => {
  if (fromState === null && init) {
    const { store } = dependencies
    init({ dispatch: store.dispatch, state: store.getState() })
  }

  done()
}

// router factory
export default (routeConfig: IRouteConfig) => {
  const { routes, defaultRoute, base, init } = routeConfig
  const router = createRouter(routes, { ...defaultRoute })

  // plugins
  router.usePlugin(browserPlugin({ base }))

  // middlewares
  router.useMiddleware(onRouteChangeMiddleware(routes), onRouteInitMiddleware(init))

  return router
}
