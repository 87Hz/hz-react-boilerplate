import axios from 'axios'

/**
 * axios instance
 */
export const axiosClient = axios.create({
  baseURL: 'http://www.mocky.io/v2/5a68a7ca2e0000b724d5b517',
  timeout: 1000,
})
