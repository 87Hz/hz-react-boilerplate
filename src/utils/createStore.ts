import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { router5Middleware, router5Reducer, RouterState } from 'redux-router5'
import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import { Router } from 'router5'

/**
 * load models
 */
import counter, { ICounterState } from 'src/models/counter'
import loading, { ILoadingState } from 'src/models/loading'
import remote, { IRemoteState, sagaWatcher as remoteSagaWatcher } from 'src/models/remote'

/**
 * store state type
 */
export interface IAppState {
  counter: ICounterState
  loading: ILoadingState
  remote: IRemoteState
  router: RouterState
}

/**
 * root reducer
 */
const rootReducer = combineReducers({
  counter,
  loading,
  remote,
  router: router5Reducer,
})

/**
 * root saga
 */
const rootSaga = function*() {
  yield all([...remoteSagaWatcher])
}

/**
 * export store as default
 */
export default (router: Router) => {
  const mRouter5Middleware = router5Middleware(router)
  const mSagaMiddleware = createSagaMiddleware()

  const storeEnhancer = applyMiddleware(mRouter5Middleware, mSagaMiddleware)

  const store =
    process.env.NODE_ENV !== 'production'
      ? createStore(rootReducer, composeWithDevTools(storeEnhancer))
      : createStore(rootReducer, storeEnhancer)

  mSagaMiddleware.run(rootSaga)

  return store
}
