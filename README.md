# Document

## Tools used

* prettier: auto format code. For Visual Studio Code, install Extension: Prettier - Code formatter

## Tasks

### In progress

### Plan

* Backend GraphQL: Backend TS + Restify + GraphQL (Prisma?), Go.

### Done

* 添加 webpack 对 jsx 的支持（某些库的 ts 类型不完善）
* 升级到 webpack 4
* MutltiEntry 的解决方案需要服务器端的 URL Rewrite，所以需要 nginx 来反向代理。用 Docker 来实现前端代码的部署。
* 增加 Less 的支持
* Routing (router5)
* Async actions
* Add back TSLint as prettier does not check some rules like (noused-var)
* Global/Screen Loading status（看看 dva 中 saga 的解决方案）
* 重新回到 MultiEntry 的解决方案
