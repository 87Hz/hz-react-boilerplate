/**
 * 默认样式变量
 * https://github.com/ant-design/ant-design-mobile/blob/master/components/style/themes/default.less
 * 
 */

export const theme = {
  'brand-primary': 'red',
  'color-text-base': '#333'
}