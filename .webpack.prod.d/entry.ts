import { Configuration, Entry } from 'webpack'

import entries from '../.webpack.d/entries'
import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const entry: Entry = {}

  entries.forEach(({ name }) => (entry[name] = ['babel-polyfill', `./src/entries/${name}/index.tsx`]))

  configuration.entry = entry
}

export default extender
