import CopyWebpackPlugin from 'copy-webpack-plugin'
import { Configuration, Plugin } from 'webpack'

import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const plugin = new CopyWebpackPlugin([
    {
      from: 'public',
      to: '',
    },
  ])

  configuration.plugins.push(plugin)
}

export default extender
