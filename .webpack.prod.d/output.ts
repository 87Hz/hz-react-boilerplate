import { join } from 'path'
import { Configuration, Output } from 'webpack'

import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const output: Output = {
    filename: 'bundle/[name].[chunkhash:8].js',
    path: join(__dirname, '..', 'dist'),
    publicPath: '/',
    sourceMapFilename: 'bundle/[name].[chunkhash:8].map',
  }

  configuration.output = output
}

export default extender
