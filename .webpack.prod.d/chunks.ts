import path from 'path'
import { Configuration, Options } from 'webpack'

import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const splitChunks: Options.SplitChunksOptions = {
    chunks: 'all',
    name: 'common',
  }

  configuration.optimization.splitChunks = splitChunks
}

export default extender
