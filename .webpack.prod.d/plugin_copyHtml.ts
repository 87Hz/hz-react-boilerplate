import * as HtmlWebpackPlugin from 'html-webpack-plugin'
import { Configuration, Plugin } from 'webpack'

import entries from '../.webpack.d/entries'
import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  entries.forEach(({ name, title }) => {
    const plugin: Plugin = new HtmlWebpackPlugin({
      chunks: ['common', name],
      filename: `${name}.html`,
      template: 'src/template.html',
      title,
    })

    configuration.plugins.push(plugin)
  })
}

export default extender
