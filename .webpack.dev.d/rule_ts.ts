import { Configuration, Rule } from 'webpack'

import { ConfigurationExtender } from '../webpack.config'

// All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader' first, then babel-loader
// cache-loader is only to improve the performance which should not be used for production
const extender: ConfigurationExtender = (configuration: Configuration) => {
  const rule: Rule = {
    test: /\.tsx?$/,
    use: [
      'cache-loader',
      {
        loader: 'babel-loader',
        options: {
          compact: true,
          plugins: [
            'react-hot-loader/babel',
            ['import', { style: true, libraryName: 'antd-mobile' }],
            [
              'transform-imports',
              {
                lodash: { transform: 'lodash/${member}', preventFullImport: true },
                'lodash/fp': { transform: 'lodash/fp/${member}', preventFullImport: true },
                recompose: { transform: 'recompose/${member}', preventFullImport: true },
              },
            ],
          ],
          presets: [['env', { modules: false }], 'stage-0'],
        },
      },
      'ts-loader',
    ],

    exclude: /node_modules/,
  }

  configuration.module.rules.push(rule)
}

export default extender
