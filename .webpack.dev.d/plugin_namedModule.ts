import { Configuration, NamedModulesPlugin, Plugin } from 'webpack'

import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const plugin: Plugin = new NamedModulesPlugin()
  configuration.plugins.push(plugin)
}

export default extender
