import { Configuration, Rule } from 'webpack'

import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const rule: Rule = {
    test: /\.css$/,
    use: ['style-loader', 'css-loader'],
  }

  configuration.module.rules.push(rule)
}

export default extender
