/**
 * -------------------------------------------------
 * ONLY CHANGE CODE BELOW IN CASE OF MORE ENTRIES
 * 如果有新的入口，修改以下部分的代码
 * -------------------------------------------------
 */

interface IEntry {
  name: string
  title: string
}

const entries: IEntry[] = [{ name: 'index', title: 'Pre Login' }, { name: 'post-login', title: 'Post Login' }]

export default entries
