import { Configuration, Rule } from 'webpack'

import { theme } from '../theme'
import { ConfigurationExtender } from '../webpack.config'

const extender: ConfigurationExtender = (configuration: Configuration) => {
  const rule: Rule = {
    test: /\.less$/,
    use: ['style-loader', 'css-loader', { loader: 'less-loader', options: { modifyVars: theme } }],

    include: /node_modules/,
  }

  configuration.module.rules.push(rule)
}

export default extender
